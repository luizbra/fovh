#!/bin/sh -e

BASEDIR="$(dirname "$(realpath "$0")")"

[ "$(id -u)" -ne 0 ] && {
    echo 'This script needs ROOT privileges.'
    exit 1
}

kill_daemon() {
    [ -n "$(pgrep openvpn)" ] && pkill openvpn; :
    echo 'OpenVPN daemon killed.'
}

init_daemon() {
    kill_daemon
    sleep 1
    (cd "$BASEDIR" && sed -e 's/^\(auth-user-pass\).*/\1 credentials.txt/' -e '$a daemon' "$1" | openvpn /dev/stdin)
    echo 'OpenVPN daemon initalized.'
}

setup_forwarding() {
    echo 1 > /proc/sys/net/ipv4/ip_forward
    sudo iptables -t nat -A POSTROUTING -o tun0 -j MASQUERADE
    sudo iptables -A FORWARD -i tun0 -o "$1" -m state --state RELATED,ESTABLISHED -j ACCEPT
    sudo iptables -A FORWARD -i "$1" -o tun0 -j ACCEPT
    echo 'IP forwarding enabled.'
}

get_status() {
    if [ -n "$(pgrep openvpn)" ]; then
        echo 'Connected'
        exit 0
    else
        echo 'Disconnected'
        exit 1
    fi
}

run_webapp() {
    [ -f  "$BASEDIR/.venv/bin/python" ] && (cd "$BASEDIR" &&
        "$BASEDIR/.venv/bin/python" fovh.py)
}

usage() {
    cat << EOF
Usage:

    -d ovpn_file    Initialize a OpenVPN daemon with specified ovpn file
    -f interface    Setup IP forwarding for specified interface
    -k              Kill OpenVPN daemon
    -r              Run the webapp (on port 8080)
    -s              Get OpenVPN daemon status
EOF
}

while getopts ':d:f:krs' opt; do
    case "$opt" in
        d) init_daemon "$OPTARG" ;;
        f) setup_forwarding "$OPTARG" ;;
        k) kill_daemon ;;
        r) run_webapp ;;
        s) get_status ;;
        *) usage ; exit 1 ;;
    esac
done
