import json
import os

from flask import Flask, redirect, render_template, request, url_for
import netifaces
import requests

app = Flask(__name__)
app.config['INTERFACE'] = 'eth0'
app.config['ADDRESS'] = netifaces.ifaddresses(app.config['INTERFACE'])[netifaces.AF_INET][0]['addr']
app.config['HELPER'] = os.path.join(app.root_path, 'helper.sh')
app.config['OVPNS_DIR'] = os.path.join(app.root_path, 'ovpns')

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST' and request.form:
        ovpn = request.form.get('ovpn')
        if os.path.isfile(os.path.join(app.config['OVPNS_DIR'], ovpn)):
            os.system(f"{app.config['HELPER']} -d {os.path.join(app.config['OVPNS_DIR'], ovpn)}")

    remote_ip = requests.get('https://ifconfig.me')
    if remote_ip.status_code == 200:
        remote_ip = remote_ip.text
        geolocation = requests.get(f'http://ip-api.com/json/{remote_ip}')
        if geolocation.status_code == 200:
            geolocation = json.loads(geolocation.text)
            geolocation = f"{geolocation['city']}, {geolocation['country']}"
        else:
            geolocation = 'Unavailable'
    else:
        remote_ip = 'Unavailable'
        geolocation = 'Unavailable'

    status = os.system(f"{app.config['HELPER']} -s")
    if status == 0:
        status = 'Connected'
    else:
        status = 'Disconnected'

    return render_template(
        'index.html',
        local_ip=app.config['ADDRESS'],
        remote_ip=remote_ip,
        geolocation=geolocation,
        status=status,
        ovpns=os.listdir(app.config['OVPNS_DIR'])
    )

@app.route('/disconnect')
def disconnect():
    os.system(f"{app.config['HELPER']} -k")
    return redirect(url_for('index'))

if __name__ == '__main__':
    app.run(host=app.config['ADDRESS'], port=8080)
