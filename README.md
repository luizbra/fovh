# Flask OpenVPN Helper

## How to setup

1. Put your VPN login and password (one per line) in a file called `credentials.txt`
2. Setup IP forwarding (non persistent)

        sudo ./helper.sh -f INTERFACE

3. Create the virtual environment and install the required packages

        python3 -m venv .venv
        source .venv/bin/activate
        pip install -r requirements.txt

4. Place your OVPNs into the configured folder (by default is `ovpns`)
5. Run the webapp as ROOT

        sudo ./helper.sh -r

## Screenshot

![screenshot](https://codeberg.org/luizbra/fovh/raw/branch/master/screenshot.jpeg)
